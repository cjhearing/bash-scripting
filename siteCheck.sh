#!/bin/bash
#Script assumes that mail.txt and siteLog.txt exists in the same directory
clear

destination=#email
email=#email:password
now=$(date)
flag=0

sites=()

echo "" > ./mail.txt #clear mail.txt 

for i in "${sites[@]}"
do
    status=$(curl -s $i -I | head -n 1 | cut -c 7-11)
    domain=$(echo $i | cut -c 12-)
    upMessage="$now -- $domain is up with status $status"
    downMessage="$now -- $domain is down with status $status"
    mail="$domain is down with status $status"


    if [[ $status -eq 200 ]];
    then 
        echo "$i is up"
        echo $upMessage >> ./siteLog.txt #print to log
    else 
        echo "$i is down"
        echo $downMessage >> ./siteLog.txt #print to log
        echo $mail >> mail.txt #append to the mail to be sent
        flag=1
    fi
done

#check if mail.txt is empty or not
if [ $flag -eq 1 ]
then
    curl --url "smtps://smtp.gmail.com:465" --ssl-reqd --mail-from $email --mail-rcpt $destination --upload-file mail.txt --user $email --insecure; 
fi
echo "SCAN COMPLETE" >> ./siteLog.txt