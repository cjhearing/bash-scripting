## MadLibs Game 

### [Phase 1](https://bitbucket.org/cjhearing/bash-scripting/src/master/madlibs/phase1.sh)

* Prompt user for series of words and print the story to the screen.

### [Phase 2](./madlibs/phase2.sh)

* Print story to stdout and save to external file.

### [Phase 3](./madlibs/phase3.sh)

* Verfiy if user is over 18 years.

### [Phase 4](./madlibs/phase4.sh)

* Randomly generate different stories with external files.

### [Phase 5](./madlibs/phase5.sh)

* Create x number of stories as designated by the user, attribute to random names and save to external file.

## Other Scripts

### [FileGenerator](./fileGen.sh)

* Takes file name, type, size, and destination as input, outputs desired number of test files of the desired size.

### [SiteCheck](./siteCheck.sh)

* Checks if given sites are returning a 200 message, logs the results, and sends an email if any of the sites are not resolving to 200.

### [txtBomb](./txtBomb.sh)

* (Just for fun) Takes a phone number and email auth as input, spam sends a desired message x times.

### [BlackJack](./blackJack.sh)

* Play against the dealer to get as close to 21 as you can.