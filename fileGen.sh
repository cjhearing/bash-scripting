#!/bin/bash
clear
echo "

___________.__.__             ________                       ____ 
\_   _____/|__|  |   ____    /  _____/  ____   ____   ___  _/_   |
 |    __)  |  |  | _/ __ \  /   \  ____/ __ \ /    \  \  \/ /|   |
 |     \   |  |  |_\  ___/  \    \_\  \  ___/|   |  \  \   / |   |
 \___  /   |__|____/\___  >  \______  /\___  >___|  /   \_/  |___|
     \/                 \/          \/     \/     \/              

"

echo "-----------------------------File attribute----------------------------------------------"
read -p "Number of files: " nfiles
read -p "File size (xG,M,B): " fsize
read -p "File name: " fname
read -p "File type (txt, pdf): " extension
read -p "New directory name: " directory
echo ""
read -p "You are about to create $nfiles $fsize files... Continue?(Y/N): " confirm

if [ $confirm == Y ] || [ $confirm == y ]
then
    echo "Starting..."
    mkdir ~/Desktop/$directory
    while [ $nfiles -gt 0 ]
    do
        mkfile -n $fsize ~/Desktop/$directory/$fname$nfiles.$extension
        echo "Generating file $nfiles in ~/Desktop/$directory"
        nfiles=$((nfiles - 1))  
    done 
    echo ""
    echo "Done!"
    read -p "Show in folder?(Y/N): " confirm
    if [ $confirm == Y ] || [ $confirm == y ]
    then
        echo "Printing contents of ~/Desktop/$directory"
        echo "-----------------------------------------------------------------------------------"
        ls ~/Desktop/$directory/
        echo "-----------------------------------------------------------------------------------"
    fi
fi
echo ""
echo "Bye!"