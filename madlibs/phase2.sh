#!/bin/bash

clear

echo "===================================================================="
echo "                       Welcome to MadLibs"
echo "===================================================================="

read -p "What is your name?: " userName

echo "------------------------------------------------"

read -p "Please provide an ADJECTIVE: " adjective1
read -p "Please provide a NOUN: " noun1
read -p "Please provide a plural NOUN: " noun2
read -p "Please provide a VERB: " verb1
read -p "Please provide a plural NOUN: " noun3

echo "------------------------------------------------"

madLib="My company is developing a $adjective1 $noun1 to help $noun2 $verb1 $noun3."
echo ${madLib}

# Print username to new file with users name.
echo "Username: $userName" > /home/cjh14d/scripts/madlibs/$userName\sstory.txt
# Print story to same file.
echo ${madLib} >> /home/cjh14d/scripts/madlibs/$userName\sstory.txt
