#!/bin/bash
clear

adj="/home/cjh14d/scripts/madlibs/word-lists/adjectives.txt"
noun="/home/cjh14d/scripts/madlibs/word-lists/nouns.txt"
pnoun="/home/cjh14d/scripts/madlibs/word-lists/pnouns.txt"
verb="/home/cjh14d/scripts/madlibs/word-lists/verbs.txt"

function wordGrab () {
  shuf -n 1 "$1"
}

echo "===================================================================="
echo "                       Welcome to MadLibs"
echo "===================================================================="

read -p "Please enter your age: " userAge

if [ $userAge -ge 18 ]
  then
    read -p "What is your name?: " userName
    # Print username to new file with users name.
    echo "Username: $userName" > /home/cjh14d/scripts/madlibs/$userName\sstory.txt
  	echo "------------------------------------------------"
  	madLib="My company is developing a $(wordGrab $adj) $(wordGrab $noun) to help $(wordGrab $pnoun) $(wordGrab $verb) $(wordGrab $pnoun)."
  	echo ${madLib}
  	echo ${madLib} >> /home/cjh14d/scripts/madlibs/$userName\sstory.txt

  else
  	echo ""
  	echo "Sorry, you must be 18 years or older to play"
  	echo ""
  	echo "Exiting MadLibs..."
fi
