#!/bin/bash
clear
#Generate the dealers inital hand
j=2
dealer=() #dealer cards
dealer[0]=$((RANDOM%11+2))
dealer[1]=$((RANDOM%11+2))
dtotal=$(echo "${dealer[@]/%/+}0" | bc)

#Assume dealer hits until total is greater than 16
while [ $dtotal -le 16 ]
do
    dealer[$j]=$((RANDOM%11+2))
    dtotal=$(echo "${dealer[@]/%/+}0" | bc)
    j=$((j+1))
done

#Deal 2 cards to the player, random between 2 - 11 
#Assuming that the ace is valued at 11
player=()
player[0]=$((RANDOM%11+2))
player[1]=$((RANDOM%11+2))
i=2

echo "Your cards are ${player[0]} and ${player[1]}."
echo ""
echo "The dealer has a ${dealer[0]} and a face down card."
echo ""
total=$(echo "${player[@]/%/+}0" | bc)
echo "Your total = $total"
echo ""

while [ $total -le 21 ];
do
    read -p "A: Hit B: Stay: " action
    if [ $action == A ] || [ $action == a ];
    then
        player[$i]=$((RANDOM%11+2))
        echo "Your cards are now ${player[*]}"
        total=$(echo "${player[@]/%/+}0" | bc)
        echo "Total = $total"
        echo ""
        i=$((i+1))
    elif [ $action == B ] || [ $action == b ];
    then
        echo "Stay"
        break
    else
        echo "Please select A or B"
    fi
done

#Evaluate win condition
echo "----------------------------------------------"
echo "Your cards are  ${player[*]}"
echo "The dealers cards are ${dealer[*]} "
echo "Dealers total = $dtotal"
echo "Your total = $total"
echo "----------------------------------------------"
if [ $total -le 21 ];
then
    if [ $total -ge $dtotal ] || [ $dtotal -gt 21 ];
    then
        echo "You win!"
    elif [ $dtotal > $total ];
    then
        echo "You lose"
    else
        echo "Something is wrong I can feel it..."
    fi
else 
    echo "You lose, Your total was greater than 21"
fi